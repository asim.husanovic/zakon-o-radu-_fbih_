# MOJA licenca

Copyright © 2021 Asim Husanović

Ovim se daje besplatna dozvola bilo kojoj osobi koja dobije kopiju ovog repozitorija i pripadajućih datoteka sa dokumentacijom (dalje u tekstu: Repozitorij) za rad sa Repozitorijem bez ograničenja, uključujući bez ograničenja prava na upotrebu, kopiranje, modifikovanje, spajanje, objavljivanje, distribuiranje, podlicenciranje i/ili prodavanje kopije Repozitorija i omogućava osobama kojima je Repozitorij dostavljen da to isto čine, pod sljedećim uslovima:

Gornja obavijest o autorskim pravima i ova obavijest o dozvoli bit će uključene u sve kopije ili značajne dijelove repozitorija.

REPOZITORIJ SE DOSTAVLJA "KAKAV JEST", BEZ GARANCIJA BILO KOJE VRSTE, IZRIČITE ILI PODRAZUMIJEVANE, UKLJUČUJUĆI, ALI NE ograničavajući se na GARANCIJE PRODAJE, SPOSOBNOST ZA ODREĐENU SVRHU I NEMOĆ. AUTORI ILI NOSITELJI AUTORSKIH PRAVA NIKADA NE ODGOVARAJU ZA BILO KOJU POTRAŽNJU, ŠTETU ILI DRUGU ODGOVORNOST, BILO U AKCIJI UGOVORA, DELITETSKOG ILI DRUGOG PROIZVODA IZ, VAN ILI U VEZI S REPOZITORIJEM ILI UPOTREBOM ILI BILO KOJIM DRUGIM KORIŠTENJEM REPOZITORIJA.
