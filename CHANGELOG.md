# Praćenje promjena
Sve značajne promjene na ovom projektu bit će dokumentovane u ovoj datoteci.

Format se zasniva na [Čuvanje promjena - hrvatski](https://keepachangelog.com/hr/1.0.0/),
kao i na pridržavanju [Semantičko verzioniranje - hrvatski](https://semver.org/lang/hr/).

## [U izradi - još uvijek neobjaveljno u Službenim novinama]

## [0.0.1] - 2021-07-07
### Added
- Ova datoteka (CHANGELOG) da bi sve teklo prema standardima
- Datoteka README u sa osnovnim obavještenjima i opisom repozitorija
- LICENSE datoteka s primjerom kako bi jedna licenca trebala da izgleda
- CONTRIBUTING datoteka s listom subjekata koji učestvuju u aktivnostima repozitorija


[U izradi - još uvijek neobjaveljno u Službenim novinama]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.1.1...HEAD
[0.0.1]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.0.1
