# Zakon o radu (Federacije Bosne i Hercegovine)

Ovaj GitLab repozitorij je napravljen u svrhu demonstracije kako se može jednostavno, brzi i veoma praktično koristiti neki od [VCS](https://hr.wikipedia.org/wiki/Upravljanje_izvornim_k%C3%B4dom) u nesoftverske svrhe.

Repozitorij će sadržavati samo Zakon o radu (Federacije Bosne i Hercegovine), koji smo uspjeli naći na internetu ([Zakon o radu bos](http://www.fbihvlada.gov.ba/bosanski/zakoni/1999/zakoni/zakoni%20x/zak%20o%20radu%20bos.htm))

Svaki repozitorij koji se objavljuje u većini VCS-ova, poput ovog (GitLab) treba da sadržava i neke druge datoteke kao što su:

- README.md - Ovaj dokument u kome je opisano za šta ovaj repozitorij služi, te kako se repozitorij koristi, koja su prava, ko su autori, licence i tako dalje
- [CHANGELOG.md](CHANGELOG.md) - Dokument u kome se vodi evidencija o promjenama na repozitoriju prema standardu [Čuvanje promjena - hrvatski](https://keepachangelog.com/hr/1.0.0/)
- [LICENSE.md](LICENSE.md) - Dokument u kome je opisana licenca za prava korištenja i uređjivanja repozitorija
- [CONTRIBUTING.md](CONTRIBUTING.md) - Dokument u kome se nalazi lista aktivnih subjekata koji rade na izradi ili promjenama repozitorija, te na koji način se može doprinijeti ovom repozitoriju

**Napomena:**
- Svijet računara se vrti oko engleskog jezika, zbog toga su nazivi određenih, standardnih datoteka sačuvane na engleskom jeziku. Moguće je napraviti određene modifikacije, ali one zahtijevaju naporan rad koji ne polučuje uvijek željene rezultate.
