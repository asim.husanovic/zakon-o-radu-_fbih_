# ZAKON O RADU

## I - OSNOVNE ODREDBE

### Član 1.

Ovim zakonom uređuje se zaključivanje ugovora o radu, radno vrijeme, plaće, prestanak ugovora o radu, ostvarivanje prava i obaveza iz radnog odnosa, zaključivanje kolektivnih ugovora, mirno rješavanje kolektivnih radnih sporova i druga pitanja iz radnog odnosa, ako drugim zakonom nije drugačije određeno.

### Član 2.

Zaključivanjem ugovora o radu između poslodavca i zaposlenika zasniva se radni odnos.

### Član 3.

Poslodavac, u smislu ovog zakona, je fizičko ili pravno lice koje zaposleniku daje posao, te mu za obavljeni rad isplaćuje plaću i izvršava druge obaveze prema zaposleniku u skladu sa ovim zakonom, propisom kantona, kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

### Član 4.

Zaposlenik, u smislu ovog zakona, je fizičko lice koje u radnom odnosu lično obavlja određene poslove za poslodavca i po tom osnovu ostvaruje prava i obaveze u skladu sa ovim zakonom, propisom kantona, kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

### Član 5.

Lice koje traži zaposlenje, kao i lice koje se zaposli, ne može biti stavljeno u nepovoljniji položaj zbog rase, boje kože, pola, jezika, vjere, političkog ili drugog mišljenja, nacionalnog ili socijalnog porijekla, imovnog stanja, rođenja ili kakve druge okolnosti, članstva ili nečlanstva u političkoj stranci, članstva ili nečlanstva u sindikatu, te tjelesnih i duševnih poteškoća.

### Član 6.

Zaposlenik ima pravo na zdravstvenu zaštitu i druga prava u slučaju bolesti, smanjenja ili gubitka radne sposobnosti i starosti, kao i pravo na druge oblike socijalne sigurnosti, u skladu sa zakonom.

### Član 7.

Zaposlenik - žena ima pravo i na posebnu zaštitu za vrijeme trudnoće, porođaja i materinstva.
Zaposlenik stariji od 15, a mlađi od 18 godina (u daljem tekstu: maloljetnik), uživa posebnu zaštitu.

### Član 8.

Zaposlenik, kome prestane radni odnos, prijavom službi za zapošljavanje, ostvaruje pravo na zdravstvenu zaštitu u slučaju bolesti i invalidnosti, pravo na materijalno osiguranje i druga prava za vrijeme nezaposlenosti, u skladu sa zakonom.

### Član 9.

Zaposlenici imaju pravo, po svom slobodnom izboru organizirati sindikat, te se u njega učlaniti, u skladu sa statutom ili pravilima tog sindikata.
Poslodavci imaju pravo, po svom slobodnom izboru, da formiraju udruženje poslodavaca, te da se u njega učlane, u skladu sa statutom ili pravilima tog udruzenja. Sindikat i udruženja poslodavaca mogu se osnovati bez ikakvog prethodnog odobrenja.

### Član 10.

Zaposlenici odnosno poslodavci slobodno odlučuju o svom stupanju ili istupanju iz sindikata, odnosno udruženja poslodavaca.
Zaposlenik odnosno poslodavac ne može biti stavljen u nepovoljniji položaj zbog članstva ili nečlanstva u sindikatu odnosno udruženju poslodavaca.

### Član 11.

Djelatnost sindikata odnosno udruženja poslodavaca ne može se trajno ni privremeno zabraniti.

### Član 12.

Pitanja iz oblasti radnih odnosa, uređuju se i propisom kantona, u skladu sa ovim zakonom.

### Član 13.

Na sva pitanja koja su u vezi sa ugovorom o radu, a koja nisu uređena ovim ili drugim zakonom, primjenjuju se opći propisi obligacionog prava.

## II - ZAKLJUČIVANJE UGOVORA O RADU

### Član 14.

Ugovor o radu zakljucuje se u pismenoj formi. Ako poslodavac ne zakljuci sa zaposlenikom ugovor o radu u pismenoj formi ili mu u roku od 15 dana od dana pocetka rada ne uruci pismenu potvrdu o zakljucenom ugovoru, smatra se da je sa zaposlenikom zakljucio ugovor o radu na neodredeno vrijeme.
Poslodavac moze u roku od jedne godine dokazati da ugovor o radu nije zakljucio sa zaposlenikom, odnosno da zaposlenik u smislu stava 2. ovog clana nikada nije radio kod poslodavca. Potvrda iz stava 2. ovog clana mora sadrzavati osnovne podatke iz ugovora o radu, posebno one o stranama, pocetku rada i placi.

### Član 15.

Ugovor o radu moze da zakljuci lice koje je navrsilo 15 godina zivota i koje ima opcu zdravstvenu sposobnost utvrdenu od nadlezne zdravstvene ustanove.Maloljetnik ne moze zakljuciti ugovor o radu za obavljanje poslova koji mogu ugroziti njegovo zdravlje, moral ili razvoj.
Smatra se da invalidno lice koje je osposobljeno za obavljanje odredenih poslova, ima zdravstvenu sposobnost za obavljanje tih poslova.

### Član 16.

Ako su zakonom, kolektivnim ugovorom i pravilnikom o radu, odredeni posebni uvjeti za zasnivanje radnog odnosa, ugovor o radu moze zakljuciti samo lice koje ispunjava te uvjete.

### Član 17.

Strani drzavljani mogu zakljuciti ugovor o radu pod uvjetima utvrdenim zakonom.
1. Probni rad

### Član 18.

Prilikom zakljucivanja ugovora o radu moze se ugovoriti probni rad.
Probni rad iz stava 1. ovog clana ne moze trajati duze od tri mjeseca.
Ako je ugovoren probni rad, otkazni rok iznosi najmanje sedam dana.
2. Ugovor o radu na neodredeno i odredeno vrijeme

### Član 19.

Ugovor o radu zakljucuje se na neodredeno vrijeme, ako ovim zakonom nije drugacije odredeno.

### Član 20.

Ugovor o radu moze se zakljuciti na odredeno vrijeme u slucajevima:
· sezonskih poslova,
· zamjene privremeno odsutnog zaposlenika,
· rada na odredenom projektu,
· privremenog povecanja obima poslova, i
· drugim slucajevima utvrdenim kolektivnim ugovorom.
Ugovor o radu zakljucen na odredeno vrijeme prestaje istekom roka utvrdenog tim ugovorom za svaki pojedinacni slucaj iz stava 1. ovog clana.
3. Sadrzaj zakljucenog ugovora o radu

### Član 21.

Ugovor o radu sadrzi, narocito, podatke o:
1. nazivu i sjedistu poslodavca ;
2. imenu, prezimenu, prebivalistu odnosno boravistu zapo-slenika;
3. trajanju ugovora o radu ;
4. danu otpocinjanja rada;
5. mjestu rada;
6. radnom mjestu na kojem se zaposlenik zaposljava i kratak opis poslova;
7. duzini i rasporedu radnog vremena;
8. placi, dodacima na placu, naknadama, te periodima isplate;
9. trajanju godisnjeg odmora;
10. otkaznim rokovima kojih se mora pridrzavati zaposlenik i poslodavac;
11. druge podatke u vezi sa uvjetima rada utvrdenim kolektivnim ugovorom.
Umjesto podataka iz stava 1. tac. 7. do 11. ovog clana, moze se u ugovoru o radu naznaciti odgovarajuci zakon, kolektivni ugovor ili pravilnik o radu, kojim su uredena ta pitanja.

### Član 22.

Ako se zaposlenik upucuje na rad u inozemstvo zakljucuje se pismeni ugovor o radu prije odlaska zaposlenika u inozemstvo.
Ugovor iz stava 1. ovog clana sadrzi, pored podataka iz clana 21. ovog zakona, i podatke o:
1. trajanju rada u inozemstvu;
2. valuti u kojoj ce se isplacivati placa i drugim primanjima u novcu i naturi na koja zaposlenik ima pravo za vrijeme rada u inozemstvu;
3. uvjetima vracanja u zemlju.
4. Podaci koji se ne mogu traziti

### Član 23.

Prilikom zakljucivanja ugovora o radu poslodavac ne moze traziti od zaposlenika podatke koji nisu u neposrednoj vezi sa prirodom radnih aktivnosti koje zaposlenik obavlja.

### Član 24.

Licni podaci zaposlenika ne mogu se prikupljati, obradivati, koristiti ili dostavljati trecim licima, osim ako je to odredeno zakonom ili ako je to potrebno radi ostvarivanja prava i obaveza iz radnog odnosa.

## III - OBRAZOVANJE, OSPOSOBLJAVANJE I USAVRSAVANJE ZA RAD

### Član 25.

Poslodavac moze, u skladu sa potrebama rada, omoguciti zaposleniku obrazovanje, osposobljavanje i usavrsavanje za rad. Zaposlenik je obavezan, u skladu sa svojim sposobnostima i potrebama rada, obrazovati se, osposobljavati i usavrsavati za rad. Poslodavac je obavezan prilikom promjena ili uvodenja novog nacina ili organiziranja rada, omoguciti zaposleniku obrazovanje, osposobljavanje ili usavrsavanje za rad. Uvjeti i nacin obrazovanja, osposobljavanja i usavrsavanja za rad iz st. 1. i 2. ovog clana ureduju se kolektivnim ugovorom ili pravilnikom o radu.

## 1. Prijem pripravnika

### Član 26.

Poslodavac moze zakljuciti ugovor o radu sa pripravnikom. Pripravnikom se smatra lice koje prvi put zasniva radni odnos u zanimanju za koje se skolovalo, radi strucnog osposobljavanja za samostalan rad. Ugovor o radu sa pripravnikom zakljucuje se na odredeno vrijeme, a najduze godinu dana, ako zakonom, propisom kantona ili ugovorom o radu nije drugacije odredeno.

### Član 27.

Nakon zavrsenog pripravnickog staza, pripravnik polaze strucni ispit, u skladu sa zakonom, propisom kantona ili pravilnikom o radu.

## 2. Volonterski rad

### Član 28.

Ako je strucni ispit ili radno iskustvo utvrdeno zakonom ili pravilnikom o radu, uvjet za obavljanje poslova odredenog zanimanja, poslodavac moze lice koje zavrsi skolovanje za takvo zanimanje, primiti na strucno osposobljavanje za samostalan rad, bez zasnivanja radnog odnosa ( volonterski rad ).
Period volonterskog rada iz stava 1. ovog clana racuna se u pripravnicki staz i u radno iskustvo kao uvjet za rad na odredenim poslovima.
Volonterski rad iz stava 1. ovog clana moze trajati najduze godinu dana, ako zakonom nije drugacije odredeno. Ugovor o volonterskom radu zakljucuje se u pismenoj formi. Nacin i trajanje volonterskog rada i polaganje strucnog ispita ureduje se zakonom, kolektivnim ugovorom ili pravilnikom o radu.
Licu, za vrijeme obavljanja volonterskog rada, osigurava se odmor u toku rada pod istim uvjetima kao i za zaposlenike u radnom odnosu i prava po osnovu osiguranja za slucaj povrede na radu i profesionalne bolesti, u skladu sa propisima o penzijskom i invalidskom osiguranju.

## IV - RADNO VRIJEME

### Član 29.

Puno radno vrijeme zaposlenika traje najduze 40 sati sedmicno.

### Član 30.

Ugovor o radu moze se zakljuciti i za rad sa nepunim radnim vremenom. Zaposlenik koji je zakljucio ugovor o radu sa nepunim radnim vremenom, moze zakljuciti vise takvih ugovora kako bi na taj nacin ostvario puno radno vrijeme. Zaposlenik koji radi sa nepunim radnim vremenom ostvaruje sva prava iz radnog odnosa kao i zaposlenik sa punim radnim vremenom, osim prava koje zavisi od duzine radnog vremena (placa, naknada i sl.), u skladu sa kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu.

### Član 31.

Na poslovima na kojima, uz primjenu mjera zastite na radu, nije moguce zastititi zaposlenika od stetnih utjecaja, radno vrijeme se skracuje srazmjerno stetnom utjecaju uvjeta rada na zdravlje i radnu sposobnost zaposlenika. Poslovi iz stava 1. ovog clana i trajanje radnog vremena utvrduju se pravilnikom o radu i ugovorom o radu, u skladu sa zakonom. Pri ostvarivanju prava na placu i drugih prava po osnovu rada i u vezi sa radom, skraceno radno vrijeme u smislu st. 1. i 2. ovog clana izjednacava se sa punim radnim vremenom.

### Član 32.

U slucaju vise sile (pozar, potres, poplava) i iznenadnog povecanja obima posla, kao i u drugim slicnim slucajevima neophodne potrebe, zaposlenik, na zahtijev poslodavca, obavezan je da radi duze od punog radnog vremena (prekovremeni rad ), a najvise do 10 sati sedmicno.
Ako prekovremeni rad zaposlenika traje duze od tri sedmice neprekidno ili vise od 10 sedmica u toku kalendarske godine, o prekovremenom radu poslodavac obavijestava organ nadlezan za poslove inspekcije rada kantona (u daljnjem tekstu: inspekcija rada kantona).
Nije dozvoljen prekovremeni rad maloljetnih zaposlenika. Trudnica, majka odnosno usvojilac sa djetetom do tri godine zivota i samohrani roditelj odnosno usvojilac sa djetetom do sest godina zivota, moze raditi prekovremeno, ako da pismenu izjavu o dobrovoljnom pristanku na takav rad.
Inspekcija rada kantona zabranit ce prekovremeni rad, uveden suprotno st. 1., 2., 3. i 4. ovog clana.

### Član 33.

Ako priroda posla to zahtijeva, puno radno vrijeme moze se preraspodjeliti tako da tokom jednog perioda traje duze, a tokom drugog perioda krace od punog radnog vremena, s tim da prosjecno radno vrijeme ne moze biti duze od 52 sata sedmicno, a za sezonske poslove najduze 60 sati sedmicno.
Ako je uvedena preraspodjela radnog vremena, prosjecno radno vrijeme tokom kalendarske godine ili drugog perioda odredenog kolektivnim ugovorom, ne moze biti duze od 40 sati u sedmici. Ako je uvedena preraspodjela radnog vremena, takvo radno vrijeme ne smatra se prekovremenim radom.

### Član 34.

Rad u vremenu izmedu 22 sata uvecer i 6 sati ujutro iduceg dana, a u poljoprivredi izmedu 22 sata i 5 sati ujutro, smatra se nocnim radom, ako za odredeni slucaj zakonom, propisom kantona ili kolektivnim ugovorom nije drugacije odredeno. Ako je rad organiziran u smjenama, osigurava se izmjena smjena, tako da zaposlenik radi nocu uzastopno najvise jednu sedmicu.

### Član 35.

Zabranjen je nocni rad zena u industriji. Zabrana iz stava 1. ovog clana ne odnosi se na zene koje obavljaju rukovodece i tehnicke poslove i zene zaposlene u zdravstvenoj ili socijalnoj sluzbi, kao i na poslodavce koji zaposljavaju samo clanove svoje obitelji. Zaposlenici - zeni moze se narediti nocni rad u industriji u slucaju zastite interesa Federacije Bosne i Hercegovine (u daljem tekstu: Federacija), ako je dobijena saglasnost federalnog ministra nadleznog za rad (u daljnjem tekstu: federalni ministar) nakon prethodnog konsultiranja sa sindikatom, poslodavcem ili vise poslodavaca odnosno udruzenjem poslodavaca. Zeni se moze narediti nocni rad i bez prethodne saglasnosti, ako je takav rad neophodan zbog vise sile ili sprjecavanja kvara na sirovinama. O nocnom radu iz stava 4. ovog clana obavjestavaju se nadlezni organ kantona i inspekcija rada kantona u roku 24 sata od uvodenja ovog rada. Ako inspekcija rada kantona ocijeni da nocni rad iz stava 4. ovog clana nije neophodan, odnosno da ne postoji visa sila ili opasnost od kvara na sirovinama, zabranit ce nocni rad.

### Član 36.

Zabranjen je nocni rad maloljetnih zaposlenika. Za maloljetne zaposlenike u industriji, rad u vremenu izmedu 19 sati uvecer i 7 sati ujutro iduceg dana, smatra se nocnim radom. Za maloljetne zaposlenike koji nisu zaposleni u industriji, rad u vremenu izmedu 20 sati uvecer i 6 sati ujutro iduceg dana, smatra se nocnim radom. Izuzetno, maloljetni zaposlenici privremeno mogu biti izuzeti od zabrane nocnog rada u slucaju havarija, vise sile i zastite interesa Federacije, na osnovu saglasnosti nadleznog organa kantona.

## V - ODMORI I ODSUSTVA

## 1. Odmori

### Član 37.

Zaposlenik koji radi u punom radnom vremenu, ima pravo na odmor u toku radnog dana u trajanju od najmanje 30 minuta.
Izuzetno, poslodavac je duzan zaposleniku, na njegov zahtjev, omoguciti odmor iz stava 1. ovog clana u trajanju od jednog sata za jedan dan u toku radne sedmice.
Vrijeme odmora iz st. 1. i 2. ovog clana ne uracunava se u radno vrijeme.
Nacin i vrijeme koristenja odmora iz st. 1. i 2. ovog clana ureduje se kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

### Član 38.

Zaposlenik ima pravo na dnevni odmor izmedu dva uzastopna radna dana u trajanju od najmanje 12 sati neprekidno.
Izuzetno, za vrijeme rada na sezonskim poslovima, zaposlenik ima pravo na odmor iz stava 1. ovog clana u trajanju od najmanje 10 sati neprekidno, a za maloljetne zaposlenike u trajanju od najmanje 12 sati neprekidno.

### Član 39.

Zaposlenik ima pravo na sedmicni odmor u trajanju od najmanje 24 sata neprekidno, a ako je neophodno da radi na dan svog sedmicnog odmora, osigurava mu se jedan dan u periodu odredenom prema dogovoru poslodavca i zaposlenika.

### Član 40.

Zaposleniku se ne moze uskratiti pravo na odmor u toku rada, dnevni odmor i sedmicni odmor.

### Član 41.

Zaposlenik, za svaku kalendarsku godinu, ima pravo na placeni godisnji odmor u trajanju od najmanje 18 radnih dana.
Maloljetni zaposlenik ima pravo na godisnji odmor u trajanju od najmanje 24 radna dana.
Zaposlenik, koji radi na poslovima na kojima se, uz primjenu mjera zastite na radu, nije moguce zastititi od stetnih uticaja, ima pravo na godisnji odmor u trajanju od najmanje 30 radnih dana.
Poslovi i trajanje odmora iz stava 3. ovog clana ureduju se zakonom, propisom kantona, kolektivnim ugovorom i pravilnikom o radu.

### Član 42.

Zaposlenik koji se prvi put zaposli ili koji ima prekid rada izmedu dva radna odnosa duzi od osam dana, stice pravo na godisnji odmor nakon sest mjeseci neprekidnog rada.
Ako zaposlenik nije stekao pravo na godisnji odmor u smislu stava 1. ovog clana, ima pravo na najmanje jedan dan godisnjeg odmora za svaki navrseni mjesec dana rada, u skladu sa kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.
Odsustvo sa rada zbog privremene nesposobnosti za rad, materinstva, vojne sluzbe i drugog odsustva koje nije uvjetovano voljom zaposlenika, ne smatra se prekidom rada iz stava 1. ovog clana.

### Član 43.

Trajanje godisnjeg odmora duze od najkraceg propisanog ovim ili drugim zakonom, ureduje se kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu. U trajanje godisnjeg odmora ne uracunava se vrijeme privremene nesposobnosti za rad, vrijeme praznika u koje se ne radi, kao i drugo vrijeme odsustvovanja sa rada koje se zaposleniku priznaje u staz osiguranja. Ako je rad organiziran u manje od sest radnih dana u sedmici, pri utvrdivanju trajanja godisnjeg odmora smatra se da je radno vrijeme rasporedeno na sest radnih dana, ako kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu nije drugacije uredeno.

### Član 44.

Godisnji odmor moze se koristiti u dva dijela. Ako zaposlenik koristi godisnji odmor u dijelovima, prvi dio koristi bez prekida u trajanju od najmanje 12 radnih dana u toku kalendarske godine, a drugi dio najkasnije do 30. juna naredne godine. Zaposlenik ima pravo koristiti jedan dan godisnjeg odmora kad on to zeli, uz obavezu da o tome obavijesti poslodavca najmanje tri dana prije njegovog koristenja.

### Član 45.

Zaposlenik se ne moze odreci prava na godisnji odmor. Zaposleniku se ne moze uskratiti pravo na godisnji odmor, niti mu se izvrsiti isplata naknade umjesto koristenja godisnjeg odmora.

## 2. Odsustva sa rada

### Član 46.

Zaposlenik ima pravo na odsustvo sa rada uz naknadu place do sedam radnih dana u jednoj kalendarskoj godini - placeno odsustvo u slucaju: stupanja u brak, porodaja supruge, teze bolesti i smrti clana uze obitelji, odnosno domacinstva. Clanom uze obitelji, u smislu stava 1. ovog clana, smatraju se: supruznici odnosno vanbracni supruznici, dijete (bracno, vanbracno, usvojeno, pastorce i dijete bez roditelja uzeto na izdrzavanje), otac, majka, ocuh, maceha, usvojilac, dedo i nana (po ocu i majci), braca i sestre. Zaposlenik ima pravo na placeno odsustvo i u drugim slucajevima i za vrijeme utvrdeno propisom kantona, kolektivnim ugovorom ili pravilnikom o radu.

### Član 47.

Poslodavac moze zaposleniku, na njegov zahtjev, odobriti odsustvo sa rada bez naknade place - neplaceno odsustvo.
Izuzetno, poslodavac je duzan omoguciti zaposleniku odsustvo do cetiri radna dana u jednoj kalendarskoj godini, radi zadovoljavanja njegovih vjerskih odnosno tradicijskih potreba, s tim da se odsustvo od dva dana koristi uz naknadu place - placeno odsustvo. Za vrijeme odsustva iz stava 1. ovog clana prava i obaveze zaposlenika koji se sticu na radu i po osnovu rada, miruju.

## VI - ZASTITA ZAPOSLENIKA

### Član 48.

Poslodavac je duzan omoguciti zaposleniku da se upozna sa propisima o radnim odnosima i propisima iz oblasti zastite na radu u roku od 30 dana od dana stupanja zaposlenika na rad. Poslodavac je duzan osposobiti zaposlenika za rad na nacin koji osigurava zastitu zivota i zdravlja zaposlenika, te sprjecava nastanak nesrece.

### Član 49.

Poslodavac je duzan da osigura potrebne uvjete za zastitu na radu kojima se osigurava zastita zivota i zdravlja zaposlenika, u skladu sa zakonom.

### Član 50.

Zaposlenik ima pravo da odbije da radi ako mu neposredno prijeti opasnost po zivot i zdravlje zbog toga sto nisu provedene propisane mjere zastite na radu i o tome je duzan odmah obavijestiti inspekciju rada kantona.

## 1. Zastita maloljetnika <a name="stav.VI.1"></a>

### Član 51.

Maloljetnik ne moze da radi na narocito teskim fizickim poslovima, radovima pod zemljom ili pod vodom, ni na ostalim poslovima koji bi mogli stetno i sa povecanim rizikom da uticu na njegov zivot i zdravlje, razvoj i moral, s obzirom na njegove psihofizicke osobine.
Federalni ministar posebnim propisom utvrdit ce poslove iz stava 1. ovog clana.
Inspektor rada kantona zabranit ce rad maloljetnika na poslovima u smislu stava 1. ovog clana.

## 2. Zastita zene i materinstva

### Član 52.

Zeni se ne moze narediti, niti se zena moze rasporediti da obavlja narocito teske fizicke poslove, radove pod zemljom ili pod vodom, te druge poslove koji, s obzirom na njene psihofizicke osobine, ugrozavaju njen zivot i zdravlje.
Izuzetno, zabrana rada iz stava 1. ovog clana ne odnosi se na zene koje obavljaju rukovodece poslove i poslove zdravstvene i socijalne zastite, studente, pripravnike i volontere koji tokom skolovanja ili strucnog osposobljavanja moraju dio vremena provesti u podzemnim dijelovima rudnika, te na zene koje povremeno moraju ulaziti u podzemne dijelove rudnika radi obavljanja poslova koji nisu fizicke prirode.

### Član 53.

Poslodavac ne moze odbiti da zaposli zenu zbog njene trudnoce, ili joj zbog tog stanja otkazati ugovor o radu, ili je, osim u slucajevima iz [člana 54.](#clan54) [stava 1](#stav.VI.1) ovog zakona, rasporediti na druge poslove.

### Član 54. <a name="clan54"></a>

Zena za vrijeme trudnoce, odnosno dojenja djeteta, moze biti rasporedena na druge poslove ako je to u interesu njenog zdravstvenog stanja koje je utvrdio ovlasteni lijecnik. Ako poslodavac nije u mogucnosti da osigura rasporedivanje zene u smislu stava 1. ovog clana, zena ima pravo na odsustvo sa rada uz naknadu place, u skladu sa kolektivnim ugovorom i pravilnikom o radu. Privremeni raspored iz stava 1. ovog clana ne moze imati za posljedicu smanjenje place zene.
Zenu, iz stava 1. ovog clana, poslodavac moze premjestiti u drugo mjesto rada, samo uz njen pismeni pristanak.

### Član 54a.

1. <a name="clan54astav1"></a> Za vrijeme proglašenja stanja prirodne ili druge nesreće, kao i za vrijeme vanrednog stanja u Federaciji, poslodavac kojem je zabranjen rad odlukom nadležnog organa ili kod kojeg je usljed navedenog stanja doško do pada prihoda od najmanje 35% u odnosu na isti period u protekloj godini prema podacima organa nadležnog za naplatu poreza, može jednostranom odlukom uputiti radnike na čekanje posla, u najdužem trajanju od tri mjeseca od dana donošenja odluke.
2. Za vrijeme čekanja posla iz stava ([1](#clan54astav1)) ovog člana, radnik ima pravo na naknadu plaće u visini koju odredi poslodavac, a koja ne može biti manja od najniže plaće u Federaciji.
3. Ukoliko radnik na čekanju posla ne bude pozvan na rad nakon isteka roka iz stava ([1](#clan54astav1)) ovog člana, prestaje mu radni odnos uz pravo na otrpeminu, koja se ostvaruje načinon propisanim članom 111. ovog zakona.
4. Čekanje posla iz ovog člana smatra se odsustvom iz člana 23. ovog zakona.

### Član 55.

Za vrijeme trudnoce, porodaja i njege djeteta, zena ima pravo na porodajno odsustvo od jedne godine neprekidno, a za blizance, trece i svako slijedece dijete zena ima pravo na porodajno odsustvo od 18 mjeseci neprekidno.
Na osnovu nalaza ovlastenog lijecnika zena moze da otpocne porodajno odsustvo 45 dana prije porodaja, a obavezno 28 dana prije porodaja.
Ako zena bez svoje krivice, na osnovu nalaza ovlastenog lijecnika, ne koristi porodajno odsustvo 28 dana prije porodaja, ima pravo da te dane iskoristi poslije porodaja. Izuzetno, zena, na osnovu svog pismenog zahtjeva, moze koristiti krace porodajno odsustvo, ali ne krace od 42 dana poslije porodaja.

### Član 56.

Otac djeteta, odnosno usvojilac moze da koristi pravo iz clana 55. st. 1. i 3. ovog zakona u slucaju smrti majke, ako majka napusti dijete ili ako je iz opravdanih razloga sprijecena da koristi to pravo.

### Član 57.

Nakon isteka porodajnog odsustva, zena sa djetetom najmanje do jedne godine zivota ima pravo da radi polovinu punog radnog vremena, a za blizance, trece i svako slijedece dijete ima pravo da radi polovinu punog radnog vremena do navrsene dvije godine zivota djeteta, ako propisom kantona nije predvideno duze trajanje ovog prava. Pravo iz stava 1. ovog clana moze koristiti i zaposlenik - otac djeteta, ako zena za to vrijeme radi u punom radnom vremenu.

### Član 58.

Nakon isteka godine dana zivota djeteta, jedan od roditelja ima pravo da radi polovinu punog radnog vremena do tri godine zivota djeteta, ako je djetetu, prema nalazu nadlezne zdravstvene ustanove, potrebna pojacana briga i njega. Pravo iz stava 1. ovog clana moze da koristi i usvojilac, odnosno lice koje se stara o djetetu, u slucaju smrti oba roditelja, ako roditelji napuste dijete ili ako ne mogu da se brinu o djetetu.

### Član 59.

Zena, koja nakon koristenja porodajnog odsustva radi puno radno vrijeme, ima pravo da odsustvuje s posla dva puta dnevno u trajanju po sat vremena radi dojenja djeteta, na osnovu nalaza ovlastenog lijecnika. Pravo iz stava 1. ovog clana zena moze koristiti do navrsene jedne godine zivota djeteta.
Vrijeme odsustva iz stava 1. ovog clana racuna se u puno radno vrijeme.

### Član 60.

Ako zena rodi mrtvo dijete ili ako dijete umre prije isteka porodajnog odsustva, ima pravo da produzi porodajno odsustvo za onoliko vremena koliko je, prema nalazu ovlastenog lijecnika, potrebno da se oporavi od porodaja i psihickog stanja prouzrokovanog gubitkom djeteta, a najmanje 45 dana od porodaja odnosno od smrti djeteta, za koje vrijeme joj pripadaju sva prava po osnovu porodajnog odsustva.

### Član 61.

Jedan od roditelja moze da odsustvuje sa rada do tri godine zivota djeteta, ako je to predvideno kolektivnim ugovorom ili pravilnikom o radu.
Za vrijeme odsustvovanja sa rada u smislu stava 1. ovog clana, prava i obaveze iz radnog odnosa, miruju.

### Član 62.

Za vrijeme koristenja porodajnog odsustva, kao i odsustva iz clana 59. ovog zakona, zaposlenik ima pravo na naknadu place, u skladu sa zakonom.
Za vrijeme rada sa polovinom punog radnog vremena iz cl. 57. i 58. ovog zakona, zaposlenik ima za polovinu punog radnog vremena za koje ne radi, pravo na naknadu place, u skladu sa zakonom.

### Član 63.

Jedan od roditelja djeteta sa tezim smetnjama u razvoju (teze hendikepiranog djeteta) ima pravo da radi polovinu punog radnog vremena, u slucaju da se radi o samohranom roditelju ili da su oba roditelja zaposlena, pod uvjetom da dijete nije smjesteno u ustanovu socijalno - zdravstvenog zbrinjavanja, na osnovu nalaza nadlezne zdravstvene ustanove. Roditelju, koji koristi pravo iz stava 1. ovog clana, pripada pravo na naknadu place, u skladu sa zakonom. Roditelju koji koristi pravo iz stava 1. ovog clana, ne moze se narediti da radi nocu, prekovremeno i ne moze mu se promijeniti mjesto rada, ako za to nije dao svoj pismeni pristanak. 3. Zastita zaposlenika privremeno ili trajno nesposobnog za rad

### Član 64.

Zaposleniku koji je pretrpio povredu na radu ili je obolio od profesionalne bolesti, za vrijeme dok je privremeno nesposoban za rad, poslodavac ne moze otkazati ugovor o radu. U slucajevima i za vrijeme iz stava 1. ovog clana, poslodavac ne moze zaposleniku otkazati ugovor o radu koji je zakljucen u skladu sa ovim zakonom i na odredeno vrijeme.

### Član 65.

Povreda na radu, bolest ili profesionalna bolest ne mogu stetno utjecati na ostvarivanje prava zaposlenika iz radnog odnosa.
Zaposlenik koji je privremeno bio nesposoban za rad zbog povrede ili povrede na radu, bolesti ili profesionalne bolesti, a za koga nakon lijecenja i oporavka nadlezna zdravstvena ustanova ili ovlasteni lijecnik utvrdi da je sposoban za rad, ima pravo da se vrati na poslove na kojima je radio prije nastupanja privremene nesposobnosti za rad ili na druge odgovarajuce poslove. Zaposlenik je duzan,u roku od tri dana od dana nastupanja nesposobnosti za rad, obavijestiti poslodavca o privremenoj nesposobnosti za rad.

### Član 66.

Ako nadlezna zdravstvena ustanova ocijeni da kod zaposlenika postoji smanjena radna sposobnost ili neposredna opasnost od nastanka invalidnosti, poslodavac mu je duzan u pismenoj formi ponuditi druge poslove za koje je zaposlenik sposoban. Zaposlenik, koji je pretrpio povredu na radu ili obolio od profesionalne bolesti, ima prednost pri strucnom obrazovanju, osposobljavanju i usavrsavanju koje organizira poslodavac.

### Član 67.

Poslodavac moze, samo uz prethodnu saglasnost vijeca zaposlenika, otkazati ugovor o radu zaposleniku kod kojega postoji smanjena radna sposobnost ili neposredna opasnost od nastanka invalidnosti.

## VII - PLACE I NAKNADE PLACA

## 1. Place

### Član 68.

Place zaposlenika utvrduju se kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

### Član 69.

Kolektivnim ugovorom i pravilnikom o radu utvrduje se najniza placa, te uvjeti i nacin njenog uskladivanja. Poslodavac kojeg obavezuje kolektivni ugovor ili pravilnik o radu, ne moze zaposleniku obracunati i isplatiti placu manju od place utvrdene kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

### Član 70.

Kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu odreduju se periodi isplate place, koji ne mogu biti duzi od 30 dana.
Prilikom isplate place poslodavac je duzan zaposleniku uruciti pismeni obracun place. Pojedinacne isplate place nisu javne.

### Član 71.

Zaposlenik ima pravo na povecanu placu za otezane uvjete rada, prekovremeni rad i nocni rad, te za rad nedjeljom i praznikom ili nekim drugim danom za koji je zakonom odredeno da se ne radi, u skladu sa kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.

## 2. Naknada place

### Član 72.

Zaposlenik ima pravo na naknadu place za period u kojem ne radi zbog opravdanih slucajeva predvidenih zakonom, propisom kantona, kolektivnim ugovorom i pravilnikom o radu (godisnji odmor, privremena nesposobnost za rad, porodajno odsustvo, placeno odsustvo i sl.).
Period iz stava 1. ovog clana za koji se naknada isplacuje na teret poslodavca, utvrduje se zakonom, propisom kantona, kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu.
Zaposlenik ima pravo na naknadu place za vrijeme prekida rada do kojeg je doslo zbog okolnosti za koje zaposlenik nije kriv (visa sila, privremeni zastoji u proizvodnji i sl.), u skladu sa kolektivnim ugovorom, pravilnikom o radu i ugovorom o radu.
Zaposlenik koji odbije da radi, jer nisu provedene propisane mjere zastite na radu, ima pravo na naknadu place u visini kao da je radio, za vrijeme dok se ne provedu propisane mjere zastite na radu, ako za to vrijeme nije rasporeden na druge odgovarajuce poslove.

## 3. Zastita place i naknade place

### Član 73.

Poslodavac ne moze, bez saglasnosti zaposlenika, svoje potrazivanje prema njemu naplatiti uskracivanjem isplate place ili nekog njenog dijela, odnosno uskracivanjem isplate naknade place ili dijela naknade place.

### Član 74.

Najvise polovica place ili naknade place zaposlenika moze se prisilno obustaviti radi ispunjenja obaveze zakonskog izdrzavanja, a za ostale obaveze moze se prisilno obustaviti najvise jedna trecina place zaposlenika.

## VIII - IZUMI I TEHNICKA UNAPREDENJA ZAPOSLENIKA

### Član 75.

Zaposlenik je duzan da obavijesti poslodavca o izumu odnosno tehnickom unapredenju kojeg je ostvario na radu ili u vezi sa radom.
Izumi odnosno tehnicka unapredenja u smislu stava 1. ovog clana, su izumi odnosno tehnicka unapredenja, odredena zakonom.
Podatke o izumu odnosno tehnickom unapredenju zaposlenik je duzan da cuva kao poslovnu tajnu koju ne moze saopciti trecem licu bez odobrenja poslodavca. Poslodavac ima pravo preceg otkupa izuma, odnosno tehnickog unapredenja iz stava 1. ovog clana, pod uvjetom da se u roku od 30 dana od dana obavjestenja iz stava 1. ovog clana, izjasni o ponudi zaposlenika.

### Član 76.

O svojem izumu koji nije ostvaren na radu ili u vezi sa radom, zaposlenik je duzan da obavijesti poslodavca, ako je izum u vezi sa djelatnoscu poslodavca, te da mu pismeno ponudi ustupanje prava u vezi sa tim izumom. Poslodavac je duzan da se u roku od mjesec dana od dana obavjestenja iz stava 1. ovog clana, izjasni o ponudi zaposlenika.
Na ustupanje prava na izum iz stava 1. ovog clana na odgovarajuci nacin primjenjuju se odredbe obligacionog prava.

## IX - ZABRANA TAKMICENJA ZAPOSLENIKA SA POSLODAVCEM

### Član 77.

Zaposlenik ne moze, bez odobrenja poslodavca, za svoj ili tudi racun sklapati poslove iz djelatnosti koju obavlja poslodavac.

### Član 78.

Poslodavac i zaposlenik mogu ugovoriti da se odredeno vrijeme, nakon prestanka ugovora o radu, a najduze dvije godine od dana prestanka tog ugovora, zaposlenik ne moze zaposliti kod drugog lica koji je u trzisnoj utakmici sa poslodavcem i da ne moze za svoj ili za racun treceg lica, sklapati poslove kojima se takmici sa poslodavcem.
Ugovor iz stava 1. ovog clana moze biti sastavni dio ugovora o radu.

### Član 79.

Ugovorena zabrana takmicenja obavezuje zaposlenika samo ako je ugovorom poslodavac preuzeo obavezu da ce zaposleniku za vrijeme trajanja zabrane isplacivati naknadu najmanje u iznosu polovine prosjecne place isplacene zaposleniku u periodu od tri mjeseca prije prestanka ugovora o radu.
Naknadu iz stava 1. ovog clana poslodavac je duzan isplatiti zaposleniku krajem svakog kalendarskog mjeseca.
Visina naknade iz stava 1. ovog clana uskladuje se na nacin i pod uvjetima utvrdenim kolektivnim ugovorom, pravilnikom o radu ili ugovorom o radu.

### Član 80.

Uvjeti i nacin prestanka zabrane takmicenja ureduju se ugovorom izmedu poslodavca i zaposlenika.

## X - NAKNADA STETE

### Član 81.

Zaposlenik koji na radu ili u vezi sa radom namjerno ili zbog krajnje nepaznje prouzrokuje stetu poslodavcu, duzan je stetu naknaditi.
Ako stetu prouzrokuje vise zaposlenika, svaki zaposlenik odgovara za dio stete koju je prouzrokovao.
Ako se za svakog zaposlenika ne moze utvrditi dio stete koju je on prouzrokovao, smatra se da su svi zaposlenici podjednako odgovorni i stetu naknaduju u jednakim dijelovima. Ako je vise zaposlenika prouzrokovalo stetu krivicnim djelom sa umisljajem, za stetu odgovaraju solidarno.

### Član 82.

Ako se naknada stete ne moze utvrditi u tacnom iznosu ili bi utvrdivanje njenog iznosa prouzrokovalo nesrazmjerne troskove, kolektivnim ugovorom ili pravilnikom o radu moze se predvidjeti da se visina naknade stete utvrduje u pausalnom iznosu, kao i nacin utvrdivanja pausalnog iznosa i organ koji tu visinu utvrduje i druga pitanja u vezi sa ovom naknadom.
Ako je prouzrokovana steta mnogo veca od utvrdenog pausalnog iznosa naknade stete, poslodavac moze zahtijevati naknadu u visini stvarno prouzrokovane stete.

### Član 83.

Zaposlenik koji na radu ili u vezi sa radom namjerno ili zbog krajnje nepaznje prouzrokuje stetu trecem licu, a stetu je naknadio poslodavac, duzan je poslodavcu naknaditi iznos naknade isplacene trecem licu.

### Član 84.

Kolektivnim ugovorom i pravilnikom o radu utvrduju se uvjeti i nacin smanjenja ili oslobadanja zaposlenika od obaveze naknade stete.

### Član 85.

Ako zaposlenik pretrpi stetu na radu ili u vezi sa radom, poslodavac je duzan zaposleniku naknaditi stetu po opcim propisima obligacionog prava.

## XI - PRESTANAK UGOVORA O RADU

## 1. Nacin prestanka ugovora o radu

### Član 86.

Ugovor o radu prestaje:
1. smrcu zaposlenika;
2. sporazumom poslodavca i zaposlenika;
3. kad zaposlenik navrsi 65 godina zivota i 20 godina staza osiguranja, ako se poslodavac i zaposlenik drugacije ne dogovore;
4. danom dostavljanja pravosnaznog rjesenja o utvrdivanju gubitka radne sposobnosti ;
5. otkazom poslodavca odnosno zaposlenika;
6. istekom vremena na koje je sklopljen ugovor o radu na odredeno vrijeme;
7. ako zaposlenik bude osuden na izdrzavanje kazne zatvora u trajanju duzem od tri mjeseca - danom stupanja na izdrzavanje kazne;
8. ako zaposleniku bude izrecena mjera bezbjednosti, vaspitna ili zastitna mjera u trajanju duzem od tri mjeseca - pocetkom primjene te mjere;
9. odlukom nadleznog suda koja ima za posljedicu prestanak radnog odnosa.

## 2. Otkaz ugovora o radu

### Član 87.

Poslodavac moze otkazati ugovor o radu uz propisani otkazni rok, u slucaju prestanka potrebe za obavljanjem odredenog posla zbog ekonomskih, tehnickih ili organizacijskih razloga, iz clana 98. ovog zakona, kao i u slucaju kada zaposlenik nije u mogucnosti da izvrsava svoje obaveze iz radnog odnosa zbog gubitka radne sposobnosti za obavljanje tih poslova.
Otkaz zbog gubitka radne sposobnosti iz stava 1. ovog clana, moguc je samo ako poslodavac ne moze zaposliti zaposlenika na druge poslove ili ga ne moze obrazovati, odnosno osposobiti za rad na drugim poslovima.

### Član 88.

Poslodavac odnosno zaposlenik moze otkazati ugovor o radu, bez obaveze postivanja propisanog otkaznog roka, kada, zbog krsenja obaveza iz radnog odnosa ili zbog neispunjavanja obaveza iz ugovora o radu, nastavak radnog odnosa nije moguc.
Prije otkazivanja ugovora o radu iz stava 1. ovog clana, poslodavac moze zaposlenika pismeno upozoriti na obaveze iz radnog odnosa i ukazati mu na mogucnost otkaza za slucaj nastavka krsenja tih obaveza.

### Član 89.

Ugovor o radu, u slucaju iz clana 88. stav 1. ovog zakona, moze se otkazati u roku od 15 dana od dana saznanja za cinjenicu zbog koje se daje otkaz.
Ugovorna strana, koja u slucaju iz clana 88. stava 1. ovog zakona, otkaze ugovor o radu, ima pravo od strane koja je kriva za otkaz da trazi naknadu stete koja je nastala neizvrsavanjem preuzetih obaveza iz ugovora o radu.

### Član 90.

Ako poslodavac otkazuje ugovor o radu zbog ponasanja ili rada zaposlenika, obavezan je omoguciti zaposleniku da iznese svoju odbranu, osim ako postoje okolnosti zbog kojih nije opravdano ocekivati od poslodavca da to ucini.

### Član 91.

Ako zaposlenik odnosno poslodavac otkazuje ugovor o radu iz razloga navedenih u clanu 88. stav 1. ovog zakona, obavezan je da dokaze postojanje opravdanog razloga za otkaz.

### Član 92.

Ako poslodavac namjerava da otkaze ugovor o radu, u smislu ovog zakona, obavezan je da o tome pribavi misljenje vijeca zaposlenika.
Ako poslodavac nije pribavio misljenje vijece zaposlenika, odluka o otkazu ugovora o radu je nistavna.

### Član 93.

Poslodavac, samo uz prethodnu saglasnost sindikata, moze otkazati ugovor o radu sindikalnom povjereniku za vrijeme obavljanja njegove duznosti i sest mjeseci nakon obavljanja te duznosti.

## 3. Forma i trajanje otkaznog roka

### Član 94.

Otkaz se daje u pismenoj formi. Poslodavac je obavezan, u pismenoj formi, obrazloziti otkaz zaposleniku. Otkaz se dostavlja zaposleniku, odnosno poslodavcu kojem se otkazuje.

### Član 95.

Otkazni rok ne moze biti kraci od 15 dana niti duzi od sest mjeseci, s tim da se konkretna duzina otkaznog roka ureduje kolektivnim ugovorom i pravilnikom o radu. Otkazni rok pocinje da tece od dana urucenja otkaza zaposleniku odnosno poslodavcu.

### Član 96.

Ako zaposlenik, na zahtijev poslodavca, prestane sa radom prije isteka propisanog otkaznog roka, poslodavac je obavezan da mu isplati naknadu place i prizna sva ostala prava kao da je radio do isteka otkaznog roka. Ako sud utvrdi da je otkaz poslodavca nezakonit, poslodavac ce vratiti zaposlenika na rad i isplatiti naknadu place u visini place koju bi zaposlenik ostvario da je radio.
Zaposlenik koji osporava dati otkaz, moze traziti da sud donese privremenu mjeru o njegovom vracanju na rad, do okoncanja sudskog spora.

## 4. Otkaz s ponudom izmjenjenog ugovora o radu

### Član 97.

Odredbe ovog zakona koje se odnose na otkaz, primjenjuju se i u slucaju kada poslodavac otkaze ugovor i istovremeno ponudi zaposleniku zakljucivanje ugovora o radu pod izmjenjenim uvjetima. Ako zaposlenik prihvati ponudu poslodavca iz stava 1. ovog clana, zadrzava pravo da pred nadleznim sudom osporava dopustenost takve izmjene ugovora.

## 5. Program zbrinjavanja viska zaposlenika

### Član 98.

Poslodavac koji zaposljava vise od 15 zaposlenika, a koji u periodu od sest mjeseci ima namjeru da zbog ekonomskih, tehnickih ili organizacijskih razloga otkaze ugovor o radu najmanje petorici zaposlenika, duzan je izraditi program zbrinjavanja viska zaposlenika.

### Član 99.

Program iz clana 98. ovog zakona sadrzi, narocito:
· razloge nastanka viska zaposlenika,
· broj i kategoriju zaposlenika koji ce se pojaviti kao visak,
· mogucnost promjene u tehnologiji i organizaciji rada u cilju zbrinjavanja viska zaposlenika,
· mogucnost zaposljavanja zaposlenika na druge poslove,
· mogucnost pronalazenja zaposlenja kod drugog poslodavca,
· mogucnost prekvalifikacije ili dokvalifikacije zaposlenika,
· mogucnost skracivanja radnog vremena.
Program iz stava 1. ovog clana poslodavac je obavezan dostaviti na konsultiranje vijecu zaposlenika i obavezan je izjasniti se vijecu zaposlenika o njegovim misljenjima i prijedlozima.
Ako kod poslodavca nije formirano vijece zaposlenika, o programu iz stava 1. ovog clana poslodavac se konsultira sa sindikatom i obavezan je da se izjasni o misljenjima i prijedlozima sindikata.
Ako programom iz stava 1. ovog clana nije moguce zaposlenicima osigurati zaposlenje, moze im se otkazati ugovor o radu.
U slucaju otkaza iz stava 4. ovog clana, poslodavac ne moze u roku od dvije godine zaposliti drugo lice koje ima istu kvalifikaciju ili isti stepen strucne spreme, osim lica iz stava 4. ovog clana, ako je to lice nezaposleno.

## 6. Otpremnina

### Član 100.

Zaposlenik koji je sa poslodavcem zakljucio ugovor o radu na neodredeno vrijeme, a kojem poslodavac otkazuje ugovor o radu nakon najmanje dvije godine neprekidnog rada, osim ako se ugovor otkazuje zbog krsenja obaveze iz radnog odnosa ili zbog neispunjavanja obaveza iz ugovora o radu od strane zaposlenika, ima pravo na otpremninu u iznosu koji se odreduje u zavisnosti od duzine prethodnog neprekidnog trajanja radnog odnosa sa tim poslodavcem.
Otpremnina iz stava 1. ovog clana utvrduje se kolektivnim ugovorom i pravilnikom o radu, s tim da se ne moze utvrditi u iznosu manjem od jedne trecine prosjecne mjesecne place isplacene zaposleniku u posljednja tri mjeseca prije prestanka ugovora o radu, za svaku navrsenu godinu rada kod tog poslodavca.
Izuzetno, umjesto otpremnine iz stava 2. ovog clana, poslodavac i zaposlenik mogu se dogovoriti i o drugom vidu naknade.
Nacin, uvjeti i rokovi isplate otpremnine iz st. 2. i 3. ovog clana, utvrduju se pismenim ugovorom izmedu zaposlenika i poslodavca.

## XII - OSTVARIVANJE PRAVA I OBAVEZA IZ RADNOG ODNOSA

## 1. Odlucivanje o pravima i obavezama iz radnog odnosa

### Član 101.

O pravima i obavezama zaposlenika iz radnog odnosa odlucuje, u skladu sa ovim zakonom, kolektivnim ugovorom i drugim propisima, poslodavac ili drugo ovlasteno lice odredeno statutom ili aktom o osnivanju.
Ako je poslodavac fizicko lice moze pismenom punomoci ovlastiti drugo poslovno sposobno punoljetno lice da ga zastupa u ostvarivanju prava i obaveza iz radnog odnosa ili u vezi sa radnim odnosom.

### Član 102.

U ostvarivanju pojedinacnih prava iz radnog odnosa zaposlenik moze zahtijevati ostvarivanje tih prava kod poslodavca, pred nadleznim sudom i drugim organima, u skladu sa ovim zakonom.

## 2. Zastita prava iz radnog odnosa

### Član 103.

Zaposlenik koji smatra da mu je poslodavac povrijedio neko pravo iz radnog odnosa, moze u roku od 15 dana od dana dostave odluke kojom je povrijedeno njegovo pravo, odnosno od dana saznanja za povredu prava, zahtjevati od poslodavca ostvarivanje tog prava.
Ako poslodavac u roku od 15 dana od dana dostavljanja zahtijeva zaposlenika iz stava 1. ovog clana ne udovolji tom zahtjevu, zaposlenik moze u daljem roku od 15 dana traziti zastitu povrijedenog prava pred nadleznim sudom.
Kolektivnim ugovorom i pravilnikom o radu, u skladu sa zakonom, moze se predvidjeti postupak mirnog rjesavanja radnog spora, u kom slucaju rok od 15 dana za podnosenje zahtjeva sudu, tece od dana okoncanja tog postupka.
Propust zaposlenika da zahtijeva naknadu stete ili drugo novcano potrazivanje iz radnog odnosa u rokovima iz st. 1.do 3. ovog clana, nema za posljedicu gubitak prava na to potrazivanje.

### Član 104.

Rjesavanje nastalog radnog spora stranke u sporu mogu sporazumno povjeriti arbitrazi.
Kolektivnim ugovorom odnosno sporazumom ureduje se sastav, postupak i druga pitanja znacajna za rad arbitraze.

### Član 105.

U slucaju promjene poslodavca ili njegovog pravnog polozaja (npr. zbog nasljedivanja, prodaje, spajanja, pripajanja, podjele, promjene oblika drustva i dr. ), ugovori o radu prenose se na novog poslodavca, u skladu sa kolektivnim ugovorom.

### Član 106.

Apsolutna zastara potrazivanja iz radnog odnosa nastupa za tri godine od dana nastanka potrazivanja, ako zakonom nije drugacije odredeno.

## XIII - PRAVILNIK O RADU

### Član 107.

Poslodavac koji zaposljava vise od 15 zaposlenika donosi i objavljuje pravilnik o radu, kojim se ureduju place, organizacija rada i druga pitanja znacajna za zaposlenika i poslodavca, u skladu sa zakonom i kolektivnim ugovorom.
O donosenju pravilnika o radu poslodavac se obavezno konsultira sa vijecem zaposlenika odnosno sindikatom.
Pravilnik iz stava 1. ovog clana objavljuje se na oglasnoj tabli poslodavca, a stupa na snagu osmog dana od dana objavljivanja.
Vijece zaposlenika odnosno sindikalni povjerenik moze od nadleznog suda zatraziti da nezakonit pravilnik o radu ili neke njegove odredbe oglasi nevazecim.

## XIV - SUDJELOVANJE ZAPOSLENIKA U ODLUCIVANJU - VIJECE ZAPOSLENIKA

### Član 108.

Zaposlenici kod poslodavca koji redovno zaposljava najmanje 15 zaposlenika, imaju pravo da formiraju vijece zaposlenika, koje ce ih zastupati kod poslodavca u zastiti njihovih prava i interesa.
Ako kod poslodavca nije formirano vijece zaposlenika, sindikat ima obaveze i ovlastenja koja se odnose na ovlastenja vijeca zaposlenika, u skladu sa zakonom.

### Član 109.

Vijece zaposlenika formira se na zahtijev najmanje 20 % zaposlenika ili sindikata.

### Član 110.

Nacin i postupak formiranja vijeca zaposlenika, kao i druga pitanja vezana za rad i djelovanje vijeca zaposlenika, ureduju se zakonom.

## XV - KOLEKTIVNI UGOVORI

### Član 111.

Kolektivni ugovor moze se zakljuciti za teritoriju Federacije, za podrucje jednog ili vise kantona, odredenu djelatnost, jednog ili vise poslodavaca.

### Član 112.

Na strani zaposlenika kod zakljucivanja kolektivnog ugovora moze biti sindikat ili vise sindikata, a na strani poslodavca moze biti poslodavac, vise poslodavaca ili udruzenje poslodavaca.
Ako je u pregovaranju i zakljucivanju kolektivnog ugovora zastupljeno vise sindikata odnosno vise poslodavaca, o zakljucivanju kolektivnog ugovora mogu pregovarati samo oni sindikati odnosno poslodavci koji imaju punomoc od svakog pojedinacnog sindikata odnosno poslodavca, u skladu sa njihovim statutom.
Do formiranja udruzenja poslodavaca na strani poslodavca kod zakljucivanja kolektivnog ugovora iz clana 111. ovog zakona, moze biti Vlada Federacije Bosne i Hercegovine (u daljnjem tekstu: Vlada Federacije ), odnosno vlada kantona.

### Član 113.

Kolektivni ugovor moze se zakljuciti na neodredeno ili odredeno vrijeme.
Kolektivni ugovor zakljucuje se u pismenoj formi.
Ako kolektivnim ugovorom nije drugacije odredeno, nakon isteka roka na koji je zakljucen, kolektivni ugovor primjenjivace se do zakljucivanja novog kolektivnog ugovora.

### Član 114.

Kolektivnim ugovorom ureduju se prava i obaveze strana koje su ga zakljucile, te prava i obaveze iz radnog odnosa ili u vezi sa radnim odnosom, u skladu sa zakonom i drugim propisima.
Kolektivnim ugovorom ureduju se i pravila o postupku kolektivnog pregovaranja, sastav i nacin postupanja tijela ovlastenih za mirno rjesavanje kolektvnih radnih sporova.

### Član 115.

Kolektivni ugovor je obavezan za strane koje su ga zakljucile, kao i za strane koje su mu naknadno pristupile.

### Član 116.

Ako postoji interes Federacije, federalni ministar moze prosiriti primjenu kolektivnog ugovora i na druga pravna lica za koja se ocjeni da je to potrebno, a koja nisu ucestvovala u njegovom zakljucivanju ili mu nisu naknadno pristupila.
Prije donosenja odluke o prosirenju vaznosti kolektivnog ugovora, federalni ministar je obavezan zatraziti misljenje sindikata, poslodavca ili vise poslodavaca odnosno udruzenja poslodavaca, na koje se kolektivni ugovor prosiruje.
Odluka o prosirenju vaznosti kolektivnog ugovora moze se opozvati na nacin utvrden za njezino donosenje.
Odluka o prosirenju vaznosti kolektivnog ugovora objavljuje se u "Sluzbenim novinama Federacije BiH".

### Član 117.

Na izmjene i dopune kolektivnih ugovora primjenjuju se odredbe ovog zakona koje vaze i za njihovo donosenje.

### Član 118.

Zakljucen kolektivni ugovor, te njegove izmjene i dopune, za teritoriju Federacije ili podrucje dva ili vise kantona dostavljaju se federalnom ministarstvu nadleznom za rad (u daljem tekstu: federalno ministarstvo), a svi ostali kolektivni ugovori dostavljaju se nadleznom organu kantona.
Postupak dostave kolektivnih ugovora iz stava 1. ovog clana federalnom ministarstvu odnosno nadleznom organu kantona, pravilnikom ce propisati federalni ministar, odnosno nadlezni kantonalni ministar.

### Član 119.

Kolektivni ugovor, zakljucen za teritoriju Federacije, objavljuje se u "Sluzbenim novinama Federacije BiH", a za podrucje jednog ili vise kantona u sluzbenom glasilu kantona.

### Član 120.

Kolektivni ugovor moze se otkazati na nacin i pod uvjetima predvidenim tim kolektivnim ugovorom.
Otkaz kolektivnog ugovora obavezno se dostavlja svim ugovornim stranama.

### Član 121.

Strane kolektivnog ugovora mogu pred nadleznim sudom zahtijevati zastitu prava iz kolektivnog ugovora.

## XVI - MIRNO RJESAVANJE KOLEKTIVNIH RADNIH SPOROVA

## 1. Mirenje

### Član 122.

U slucaju spora o zakljucivanju, primjeni, izmjeni ili dopuni odnosno otkazivanju kolektivnog ugovora ili drugog slicnog spora vezanog za kolektivni ugovor (kolektivni radni spor),ako strane nisu dogovorile nacin mirnog rjesavanja spora, sprovodi se postupak mirenja, u skladu sa ovim zakonom.
Mirenje iz stava 1. ovog clana provodi mirovno vijece.

### Član 123.

Mirovno vijece moze biti formirano za teritoriju Federacije, odnosno za podrucje kantona.
Mirovno vijece za teritoriju Federacije sastoji se od tri clana, i to: predstavnika poslodavaca, sindikata i predstavnika kojeg izaberu strane u sporu sa liste koju utvrduje federalni ministar, a formira se za period od dvije godine.
Mirovno vijece iz stava 2. ovog clana donosi pravila o postupku pred tim vijecem.
Administrativne poslove za mirovno vijece formirano za teritoriju Federacije, vrsi federalno ministarstvo.
Troskove za clana mirovnog vijeca sa liste koju utvrduje federalni ministar, snosi federalno ministarstvo.

### Član 124.

Formiranje mirovnog vijeca za podrucje kantona, njegov sastav, nacin rada i druga pitanja koja se odnose na rad tog mirovnog vijeca, ureduju se propisom kantona.

### Član 125.

Strane u sporu mogu prihvatiti ili odbiti prijedlog mirovnog vijeca, a ako ga prihvate, prijedlog ima pravnu snagu i dejstvo kolektivnog ugovora.
O ishodu i rezultatima mirenja strane u sporu obavjestavaju u roku od tri dana od dana zavrsetka mirenja federalno ministarstvo, odnosno nadlezni organ kantona, u skladu sa propisom kantona.

## 2. Arbitraza

### Član 126.

Strane u sporu mogu rjesavanje kolektivnog radnog spora sprorazumno povjeriti arbitrazi.
Imenovanje arbitara i arbitraznog vijeca i druga pitanja u vezi sa arbitraznim postupkom ureduju se kolektivnim ugovorom ili sporazumom strana.

### Član 127.

Arbitraza temelji svoju odluku na zakonu, drugom propisu, kolektivnom ugovoru i na pravicnosti.
Arbitrazna odluka mora biti obrazlozena, osim ako strane u sporu ne odluce drugacije.
Protiv arbitrazne odluke zalba nije dopustena.
Arbitrazna odluka ima pravnu snagu i dejstvo kolektivnog ugovora.

## XVII - STRAJK

### Član 128.

Sindikat ima pravo pozvati na strajk i provesti ga sa svrhom zastite i ostvarivanja ekonomskih i socijalnih prava i interesa svojih clanova.
Strajk se moze organizirati samo u skladu sa Zakonom o strajku, pravilima sindikata o strajku i kolektivnim ugovorom.
Strajk ne moze zapoceti prije okoncanja postupka mirenja predvidenog ovim zakonom, odnosno prije provodenja drugog postupka mirnog rjesavanja spora o kojem su se strane sporazumjele.

### Član 129.

Zaposlenik ne moze biti stavljen u nepovoljniji polozaj od drugih zaposlenika zbog organiziranja ili sudjelovanja u strajku, u smislu clana 128. stav 2. ovog zakona.
Zaposlenik ne moze biti ni na koji nacin prisiljavan da sudjeluje u strajku.
Ako se zaposlenik ponasa suprotno clanu 128. stav 2. ovog zakona ili ako za vrijeme strajka namjerno nanese stetu poslodavcu, moze mu se dati otkaz, u skladu sa zakonom.

## XVIII - EKONOMSKO - SOCIJALNO VIJECE

### Član 130.

U cilju ostvarivanja i uskladivanja ekonomske i socijalne politike, odnosno ineresa zaposlenika i poslodavaca, te podsticanja zakljucivanja i primjene kolektivnih ugovora i njihovog uskladivanja sa mjerama ekonomske i socijalne politike, moze se formirati ekonomsko - socijalno vijece.
Ekonomsko - socijalno vijece moze se formirati za teritoriju Federacije odnosno podrucje kantona.
Ekonomsko - socijalno vijece temelji se na trostranoj saradnji Vlade Federacije, odnosno vlade kantona, sindikata i poslodavaca.
Ekonomsko - socijalno vijece iz stava 2. ovog clana osniva se sporazumom zainteresiranih strana kojim se ureduje sastav, ovlastenja i druga pitanja od znacaja za rad ovog vijeca.
Ekonomsko - socijlano vijece iz stava 2. ovog clana donosi poslovnik o radu, kojim ce urediti nacin donosenja odluka iz svog djelokruga rada.

## XIX - NADZOR NAD PRIMJENOM PROPISA O RADU

### Član 131.

Nadzor nad primjenom ovog zakona i na osnovu njega donesenih propisa, obavlja federalni, odnosno kantonalni inspektor rada.
Ako se radi o preduzecu ili ustanovi od interesa za Federaciju, nadzor iz stava 1. ovog clana obavlja federalni inspektor rada, u skladu sa zakonom.

### Član 132.

U provodenju nadzora inspektor rada ima ovlastenja utvrdena zakonom i na osnovu zakona donesenih propisa.
Zaposlenik, sindikat, poslodavac i vijece zaposlenika mogu zahtijevati od inspektora rada provodenje inspekcijskog nadzora.

## XX - POSEBNE ODREDBE

## 1. Radna knjizica

### Član 133.

Zaposlenik ima radnu knjizicu.
Radna knjizica je javna isprava.
Radnu knjizicu izdaje opcinski organ uprave nadlezan za poslove rada.
Federalni ministar donijet ce propis o radnoj knjizici, kojim ce se urediti: sadrzaj, postupak izdavanja, nacin upisivanja podataka, postupak zamjene i izdavanja novih radnih knjizica, nacin vodenja registra izdatih radnih knjizica, oblik i nacin izrade, kao i druga pitanja predvidena propisom o radnoj knjizici.

### Član 134.

Na dan kada pocne da radi, zaposlenik predaje radnu knjizicu poslodavcu, o cemu poslodavac izdaje zaposleniku pismenu potvrdu.
Na dan prestanka ugovora o radu, poslodavac je duzan vratiti zaposleniku radnu knjizicu, a zaposlenik vraca poslodavcu potvrdu iz stava 1. ovog clana.
Vracanje radne knjizice iz stava 2. ovog clana ne moze se uvjetovati potrazivanjem koje poslodavac eventualno ima prema zaposleniku.

### Član 135.

Pored radne knjizice iz clana 134. stav 2. ovog zakona, poslodavac je duzan zaposleniku vratiti i druge dokumente i na njegov zahtjev izdati potvrdu o poslovima koje je obavljao i trajanju radnog odnosa.
U potvrdi iz stava 1. ovoga clana ne mogu se unositi podaci koji bi zaposleniku otezavali zakljucivanje novog ugovora o radu.

## 2. Privremeni i povremeni poslovi

### Član 136.

Za obavljanje privremenih i povremenih poslova moze se zakljuciti ugovor o obavljanju privremenih i povremenih poslova, pod uvjetima:
1. da su privremeni i povremeni poslovi utvrdeni u kolektivnom ugovoru ili u pravilniku o radu,
2. da privremeni i povremeni poslovi ne predstavljaju poslove za koje se zakljucuje ugovor o radu na odredeno ili neodredeno vrijeme, sa punim ili nepunim radnim vremenom i da ne traju duze od 60 dana u toku kalendarske godine.
Licu koje obavlja privremene i povremene poslove osigurava se odmor u toku rada pod istim uvjetima kao i za zaposlenike u radnom odnosu i druga prava, u skladu sa propisima o penzijskom i invalidskom osiguranju .

### Član 137.

Za obavljanje poslova iz clana 136. ovog zakona zakljucuje se ugovor u pismenoj formi.
Ugovor iz stava 1. ovog clana sadrzi: vrstu, nacin, rok izvrsenja poslova i iznos naknade za izvrseni posao.

## 3. Utjecaj vojne sluzbe na radni odnos

### Član 138.

Zaposleniku za vrijeme sluzenja vojnog roka i sluzenja u rezervnom sastavu (u daljem tekstu: vojna sluzba) prava i obaveze iz radnog odnosa miruju.
Zaposlenik koji nakon zavrsetka vojne sluzbe zeli da nastavi rad kod istog poslodavca, duzan ga je o tome obavijestiti u roku od 30 dana od dana prestanka vojne sluzbe, a poslodavac je duzan primiti zaposlenika na rad u roku od 30 dana od dana obavijesti zaposlenika.
Zaposlenika, koji je obavijestio poslodavca u smislu stava 2. ovog clana, poslodavac je duzan rasporediti na poslove na kojima je radio prije stupanja u vojnu sluzbu ili na druge odgovarajuce poslove, osim ako je prestala potreba za obavljanjem tih poslova zbog ekonomskih, tehnickih ili organizacijskih razloga u smislu clana 98. ovog zakona.
Ako poslodavac ne moze vratiti zaposlenika na rad, zbog prestanka potrebe za obavljanjem poslova u smislu stava 3. ovog clana, duzan mu je isplatiti otpremninu utvrdenu u clanu 100. ovog zakona, s tim da se prosjecna placa dovede na nivo place koju bi zaposlenik ostvario da je radio.
Ako zaposleniku prestane radni odnos u smislu stava 3. ovog clana, poslodavac ne moze u roku od godinu dana zaposliti drugo lice koje ima istu kvalifikaciju ili isti stepen strucne spreme, osim lica iz stava 1. ovog clana.
Prava utvrdena ovim zakonom u vezi sa vojnom sluzbom imaju i lica na sluzbi u rezervnom sastavu policije.

## 4. Prava zastupnika - poslanika i funkcionera

### Član 139.

Zaposleniku izabranom, odnosno imenovanom na neku od javnih duznosti, u organe Bosne i Hercegovine, Federacije Bosne i Hercegovine, organe kantona, grada i opcine i zaposleniku izabranom na profesionalnu funkciju u sindikatu, prava i obaveze iz radnog odnosa, na njegov zahtjev, miruju, a najduze cetiri godine od dana izbora odnosno imenovanja.
Na zaposlenika iz stava 1. ovog clana shodno se primjenjuju odredbe iz clana 138. st. 2. do 5. ovog zakona.

## XXI - KAZNENE ODREDBE

### Član 140.

Novcanom kaznom od 1.000,00 KM do 10.000,00, KM kaznit ce se za prekrsaj poslodavac - pravno lice, ako:
1. sa zaposlenikom ne zakljuci ugovor o radu (### Član 2.),
2. lice koje trazi zaposlenje, kao i lice koje zaposli stavlja u nepovoljniji polozaj (### Član 5.),
3. zaposleniku ne uruci pismenu potvrdu u skladu sa clanom 14.ovog zakona,
4. zakljuci ugovor o radu na odredeno vrijeme suprotno clanu 20. ovog zakona,
5. zakljuci ugovor o radu koji ne sadrzi podatke propisane ovim zakonom (### Član 21.),
6. uputi zaposlenika na rad u inozemstvo, a ugovor o radu ne sadrzi propisane podatke (### Član 22.),
7. od zaposlenika trazi podatke koji nisu u neposrednoj vezi sa radnim odnosom (### Član 23.),
8. prikuplja, obraduje, koristi ili dostavlja trecim licima podatke o zaposleniku, (### Član 24.),
9. zakljuci ugovor o radu kojim je ugovoreno trajanje pripravnickog staza duze od zakonom predvidenog (### Član 26.),
10. ugovor o volonterskom radu ne zakljuci u pismenoj formi (### Član 28.),
11. sklopi ugovor o radu u kojem je puno radno vrijeme ugovoreno u trajanju duzem od zakonom propisanog (### Član 29.),
12. od zaposlenika zahtijeva da na poslovima, na kojima se uz primjenu mjera zastite na radu nije moguce zastititi od stetnih uticaja, radi duze od skracenog radnog vremena (### Član 31).
13. od zaposlenika zahtijeva da radi duze od punog radnog vremena (prekovremeni rad) suprotno clanu 32. stav 1. ovog zakona,
14. ne obavijesti inspektora rada kantona o prekovremenom radu o kojem ga je duzan obavijestiti (### Član 32.stav 2.)
15. ne postupi po zabrani inspektora rada kantona kojim se zabranjuje prekovremeni rad (### Član 32. stav 5. ),
16. maloljetnom zaposleniku naredi prekovremeni rad (### Član 32. stav 3.),
17. trudnici, majci sa djetetom do tri godine starosti ili samohranom roditelju sa djetetom do sest godina zivota, bez njegovog pismenog pristanka, naredi prekovremeni rad (### Član 32. stav 4.),
18. zeni zaposlenoj u industriji naredi nocni rad suprotno clanu 35. ovog zakona,
19. maloljetniku naredi nocni rad (### Član 36.),
20. zaposleniku ne omoguci koristenje odmora, u toku rada (### Član 37.),
21. zaposleniku ne omoguci koristenje dnevnog i sedmicnog odmora (cl. 38. i 39.),
22. zaposleniku ne omoguci koristenje godisnjeg odmora u najkracem trajanju odredenom ovim zakonom (### Član 41. stav 1.),
23. zaposleniku uskrati pravo na godisnji odmor, odnosno isplati mu naknadu umjesto koristenja godisnjeg odmora (### Član 45.),
24. zaposleniku ne omoguci koristenje placenog odsustva (### Član 46. stav 1.),
25. rasporedi maloljetnika da radi na poslovima suprotno clanu 51. stav 1. ovog zakona,
26. zaposli zenu na poslovima na kojima je zenama zabranjen rad (### Član 52.),
27. zbog trudnoce odbije zaposliti zenu, otkaze joj ugovor o radu, ili je suprotno odredbama ovog zakona, premjesti na druge poslove (### Član 53.),
28. ne omoguci zeni odsustvo sa rada uz naknadu place (### Član 54. stav 2.),
29. premjesti zenu suprotno clanu 54. stav 4. ovog zakona,
30. ne omoguci zeni da koristi porodajno odsustvo (### Član 55.),
31. ocu djeteta odnosno usvojiocu ne omoguci koristenje prava iz clana 55. ovog zakona (### Član 56.),
32. zeni odnosno ocu djeteta ne omoguci da radi polovinu punog radnog vremena (### Član 57.),
33. jednom od roditelja odnosno usvojiocu ne omoguci da koristi pravo pod uvjetima propisanim clanom 58.ovog zakona,
34. onemoguci zeni da odsustvuje sa posla radi dojenja djeteta(### Član 59.)
35. jednom od roditelja djeteta sa tezim smetnjama u razvoju (teze hendikepiranog djeteta) odnosno usvojiocu onemoguci da koristi pravo iz clana 63. stav 1. ovog zakona,
36. otkaze zaposleniku koji je pretrpio povredu na radu ili je obolio od profesionalne bolesti, usljed cega je privremeno nesposoban za rad (### Član 64.),
37. ne vrati zaposlenika na poslove na kojima je prije radio ili na druge odgovarajuce poslove (### Član 65. stav 2.),
38. poslodavac ne ponudi zaposleniku druge poslove (### Član 66. stav 1.),
39. poslodavac ne da prednost zaposleniku pri strucnom obrazovanju, osposobljavanju i usavrsavanju (### Član 66.stav 2.),
40. poslodavac otkaze ugovor o radu zaposleniku kod kojeg postoji smanjena radna sposobnost ili neposredna opasnost od nastanka invalidnosti suprotno clanu 67. ovog zakona,
41. isplati placu manju od utvrdene kolektivnim ugovorom ili pravilnikom o radu (### Član 69. stav 2.),
42. ne isplati placu u roku utvrdenom u clanu 70. stav 1. ovog zakona,
43. poslodavac svoje potrazivanje prema zaposleniku naplati suprotno clanu 74. ovog zakona,
44. ugovor o radu prestane suprotno clanu 87. ovog zakona,
45. ne omoguci zaposleniku da iznese svoju odbranu (### Član 90.),
46. postupi suprotno clanu 93.ovog zakona,
47. ne uruci otkaz zaposleniku u pismenoj formi (### Član 94.),
48. ne ispuni obaveze prema zaposleniku iz clana 96. stav 1. ovog zakona,
49. ne izradi program zbrinjavanja viska zaposlenika u skladu sa clanom 98. ovog zakona,
50. pri izradi programa viska zaposlenika ne konsultira vijece zaposlenika odnosno sindikat (### Član 99. st. 2. i 3.),
51. zakljuci ugovor o radu sa drugim zaposlenikom (### Član 99. stav 5.),
52. poslodavac stavi u nepovoljniji polozaj zaposlenika zbog organiziranja ili sudjelovanja u strajku (### Član 129. stav 1.),
53. onemoguci ili pokusa onemoguciti federalnog odnosno kantonalnog inspektora rada u provodenju nadzora (### Član 131.),
54. odbije zaposleniku vratiti radnu knjizicu (### Član 134. stav 2.),
55. postupi suprotno odredbama cl. 136. i 137. ovog zakona,
56. u roku od sest mjeseci od dana stupanja na snagu ovog zakona ne uskladi pravilnik o radu (### Član 141.),
57. u roku od tri mjeseca iz clana 142. ovog zakona ne zakljuci sa zaposlenikom ugovor o radu,
58. zaposli drugo lice suprotno clanu 143. stav 6. ovog zakona.
Ako je prekrsaj iz stava 1. ovog clana ucinjen prema maloljetnom zaposleniku najnizi i najvisi iznos novcane kazne uvecava se dvostruko.
Za prekrsaj iz stava 1. ovog clana kaznit ce se poslodavac - fizicko lice novcanom kaznom od 500,00 KM do 2.500,00 KM.
Za prekrsaj iz stava 1. ovog clana kaznit ce se i odgovorno lice kod poslodavca koji je pravno lice, novcanom kaznom od 200,00 KM do 1.000,00 KM.

## XXII - PRIJELAZNE I ZAVRSNE ODREDBE

### Član 141.

Poslodavci su duzni uskladiti pravilnike o radu sa odredbama ovog zakona, u roku od sest mjeseci od dana njegovog stupanja na snagu.

### Član 142.

Poslodavci su duzni u roku od tri mjeseca od dana stupanja na snagu ovog zakona ponuditi zaposleniku zakljucivanje ugovora o radu, u skladu sa ovim zakonom .
Zaposlenik, kojem poslodavac ne ponudi ugovor o radu iz stava 1. ovog clana, ostaje u radnom odnosu na neodredeno odnosno odredeno vrijeme.
Ugovor iz stava 1. ovog clana ne moze biti nepovoljniji od uvjeta pod kojima je radni odnos zasnovan, odnosno pod kojima su bili uredeni radni odnosi zaposlenika i poslodavca do dana zakljucivanja ugovora iz stava 1. ovog clana, ako odredbama ovog zakona ta pitanja nisu drugacije uredena.
Ako zaposlenik ne prihvati ponudu poslodavca da zakljuci ugovor o radu u skladu sa stavom 1. ovog clana, prestaje mu radni odnos u roku od 30 dana od dana dostave ugovora o radu na zakljucivanje.
Ako zaposlenik prihvati ponudu poslodavca, a smatra da ugovor koji mu je ponudio poslodavac nije u skladu sa stavom 3. ovog clana, moze pred nadleznim sudom da ospori valjanost ponude poslodavca u roku od 15 dana od dana prihvatanja ponude.

### Član 143.

Zaposlenik koji se na dan stupanja na snagu ovog zakona zatekao na cekanju posla, ostat ce u tom statusu najduze sest mjeseci od dana stupanja na snagu ovog zakona, ako poslodavac prije isteka ovog roka zaposlenika ne pozove na rad.
Zaposlenik koji se zatekao u radnom odnosu 31. decembra 1991. godine i koji se u roku od tri mjeseca od dana stupanja na snagu ovog zakona pismeno ili neposredno obratio poslodavcu radi uspostavljanja radno-pravnog statusa, a u ovom vremenskom razdoblju nije zasnovao radni odnos kod drugog poslodavca smatrat ce se, takoder,zaposlenikom na cekanju posla.
Za vrijeme cekanja posla zaposlenik ima pravo na naknadu place u visini koju odredi poslodavac.
Ako zaposlenik na cekanju posla iz st.1.i 2. ovog clana ne bude pozvan na rad u roku iz stava 1. ovog clana, prestaje mu radni odnos, uz pravo na otpremninu najmanje u visini tri prosjecne place isplacene na nivou Federacije u prethodna tri mjeseca, koju objavljuje Federalni zavod za statistiku, do navrsenih pet godina staza osiguranja, a za svaku narednu godinu staza osiguranja, jos najmanje jednu polovinu prosjecne place.
Izuzetno, umjesto otpremnine poslodavac i zaposlenik mogu se dogovoriti i o drugom vidu naknade.
Nacin, uvjeti i rokovi isplate otpremnine iz st. 4. i 5. ovog clana, utvrduju se pismenim ugovorom izmedu zaposlenika i poslodavca.
Ako zaposleniku prestane radni odnos u smislu stava 4. ovog clana, poslodavac ne moze u roku od jedne godine zaposliti drugo lice koje ima istu kvalifikaciju ili isti stepen strucne spreme, osim lica iz st. 1.i 2. ovog clana, ako je to lice nezaposleno.

### Član 144.

Zaposlenici, ciji radni odnos miruje u skladu sa propisima koji su vazili do dana stupanja na snagu ovog zakona, a koji po ovom zakonu nemaju pravo na mirovanje radnog odnosa, imaju pravo da se vrate na poslove koje su prije obavljali ili na druge odgovarajuce poslove u roku od sest mjeseci od dana stupanja na snagu ovog zakona.

### Član 145.

Postupci ostvarivanja i zastite prava zaposlenika, zapoceti prije stupanja na snagu ovog zakona, zavrsit ce se po odredbama propisa koji su se primjenjivali na teritoriji Federacije do dana stupanja na snagu ovog zakona, ako je to za zaposlenika povoljnije.

### Član 146.

Do uredivanja placa kolektivnim ugovorom, a najduze za vrijeme od jedne godine od dana stupanja na snagu ovog zakona, Vlada Federacije utvrdit ce, uz prethodno konsultiranje strana u zakljucivanju kolektivnog ugovora, visinu, nacin i uvjete uskladivanja najnize place.
Odluka o visini najnize place iz stava 1. ovog clana objavljuje se u "Sluzbenim novinama Federacije BiH".

### Član 147.

Propisi, koji su predvideni za sprovodenje ovog zakona, donijet ce se u roku od tri mjeseca od dana stupanja na snagu ovog zakona.

### Član 148.

Do donosenja propisa iz clana 147. ovog zakona, primjenjivat ce se propisi koji su se primjenjivali do dana stupanja na snagu ovog zakona.

### Član 149.

Nadlezni organi kantona donijet ce propise, u skladu sa ovim zakonom, u roku od tri mjeseca od dana stupanja na snagu ovog zakona.

### Član 150.

Danom stupanja na snagu ovog zakona, na teritoriji Federacije prestaje primjena propisa o radnim odnosima i placama koji su se primjenjivali na teritoriji Federacije, do dana stupanja na snagu ovog zakona, osim propisa iz clana 148. ovog zakona.

### Član 151.

Ovaj zakon stupa na snagu osmog dana od dana objavljivanja u "Sluzbenim novinama Federacije BiH".
