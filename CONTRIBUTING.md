# Naš zalog

U interesu ohrabrivanja otvorenog i pristupačnog okruženja, kao saradnici i održavatelji obećavamo za sve one koji učestvuju u našem projektu i zajednici, njihovo iskustvo će biti bez uznemiravanje, bez obzira na starost, veličinu, invalidnost, nacionalnost, rodni identitet i izraz, nivo iskustva, državljanstvo, izgled, porod, vjera, ili seksualni identitet i orijentacije.

## Naši standardi

Primjeri ponašanja koji stvaraju pozitivno okruženje:

- Korištenje prijatnog i uključivog jezika
- Postovanje raznih stanovišta i iskustva
- Primanje konstruktivne kritike
- Fokusiranje na ono šoa je najbolje za zajednicu
- Pokazivanje empatije drugim članovima zajednice

Primjeri neprihvatljivog ponašanja:

- Korištenje vulgarnog jezika ili neželjene vulgarne pažnje
- Uvredljivi komentari ili lični napadi
- Javno ili privatno uznemiravanje
- Objava ličnih informacija, npr. kućna adresa bez dozvole
- Bilo kakvo drugo ponašanje koje bi se smatralo neprikladnim u profesionalnom okruženju.

## Naša odgovornost

Održavatelji projekta će razjasniti standarde za prihvatljivo ponašanje, i u slučaju neprikladnog ponašanja od njih se očekuje primjena poštenih korektivnih mjera.

Održavatelji projekta imaju pravo i odgovornost uklanjati, uređivati ili odbijati komentare, obaveze, doprinos tekstu, uređivanje wikija, izdanja i druge priloge koji nisu usklađeni s ovim Kodeksom ponašanja, a po potrebi će dostaviti razloge za odluke o moderiranju.

## Obim

Ovaj kodeks ponašanja se odnosi na sve projekte i javne prostore gdje pojedinac predstavlja zajednicu. Na primjer, korištenje službene e-mail adrese projekta ili ponašanje kao službeni predstavnik u sastancima. Održavatelji projekta će dodatno definisati ovo predstavljanje.

## Primjenjivanje

U slučaju uvredljivog ponašanja, kontaktirajte održavatelje: [E-MAIL ADRESA]

Svaka žalba će se istražiti i odgovor će biti fer i prikladan za situaciju. Održavatelji projekta su odgovorni održati povjerljivost svake žalbe. Dodatni detalji polise mogu biti objavljeni odvojeno.

Održavatelji projekta koji ne prate ili ne primijenjuju ovaj kodeks ponašanja mogu biti kažnjeni trajno ili privremeno, po nahođenje od drugih održavatelja.

## Pripisivanje

Ovaj kodeks ponašanja je preuzet iz Contributor Covenant, verzija 1.4, dostupan na https://www.contributor-covenant.org/bs/version/1/4/code-of-conduct.html
